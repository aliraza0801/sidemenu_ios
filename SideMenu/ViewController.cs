﻿using System;
using SideMenu.Menu;
using UIKit;

namespace SideMenu
{
    public partial class ViewController : BaseViewController
    {
        //protected ViewController(IntPtr handle) : base(handle)
        //{
        //    // Note: this .ctor should not contain any initialization logic.
        //}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            
            // Perform any additional setup after loading the view, typically from a nib.
        }

		protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}
