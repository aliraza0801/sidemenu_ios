using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using UIKit;

namespace SideMenu
{
    public interface SlideMenuDelegate
    {
        void slideMenuItemSelectedAtIndex(int _index);
    }
    public partial class MenuViewController : UIViewController
    {
        List<string> arrayMenuOptionsTitles = new List<string>();
        List<string> arrayMenuOptionsImages = new List<string>();

        public UIButton btnMenu;
        public SlideMenuDelegate _delegate;

        protected MenuViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            //tblMenuOptions.TableFooterView = new UIView();
            tblMenuOptions.Source = new customTableViewSource(arrayMenuOptionsTitles, arrayMenuOptionsImages, this);

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            updateArrayMenuOptions();
        }

        void updateArrayMenuOptions()
        {
            arrayMenuOptionsTitles.Add("Home");
            arrayMenuOptionsTitles.Add("My Account");

            arrayMenuOptionsImages.Add("profile");
            arrayMenuOptionsImages.Add("profile");

            tblMenuOptions.ReloadData();
        }

        partial void BtnCloseMenuOverlay_TouchUpInside(UIButton sender)
        {
            btnMenu.Tag = 0;


            if (this._delegate != null)
            {
                var index = int.Parse(sender.Tag.ToString());
                if (sender == this.btnCloseMenuOverlay)
                {
                    index = -1;
                }
                _delegate.slideMenuItemSelectedAtIndex(index);
            }

            UIView.Animate(0.3, () =>
            {


                this.View.Frame = new CGRect(-UIScreen.MainScreen.Bounds.Size.Width, 0, UIScreen.MainScreen.Bounds.Size.Width, UIScreen.MainScreen.Bounds.Size.Height);
                this.View.LayoutIfNeeded();
                this.View.BackgroundColor = UIColor.Clear;
            }, () =>
            {
                this.View.RemoveFromSuperview();
                this.RemoveFromParentViewController();
            });
        }



        class customTableViewSource : UITableViewSource
        {
            List<string> arrayMenuOptionsTitles;
            List<string> arrayMenuOptionsImages;
            MenuViewController _parent;
            public customTableViewSource(List<string> _arrayMenuOptionsTitles, List<string> _arrayMenuOptionsImages, MenuViewController parent)
            {
                arrayMenuOptionsTitles = _arrayMenuOptionsTitles;
                arrayMenuOptionsImages = _arrayMenuOptionsImages;
                _parent = parent;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                UITableViewCell cell = tableView.DequeueReusableCell("cellmenu");

                cell.SelectionStyle = UITableViewCellSelectionStyle.None;
                cell.LayoutMargins = UIEdgeInsets.Zero;
                cell.PreservesSuperviewLayoutMargins = false;
                cell.BackgroundColor = UIColor.Clear;


                UILabel lblTitle = cell.ContentView.ViewWithTag(101) as UILabel;
                UIImageView imgicon = cell.ContentView.ViewWithTag(102) as UIImageView;


                lblTitle.Text = arrayMenuOptionsTitles[indexPath.Row];
                imgicon.Image = new UIImage(arrayMenuOptionsImages[indexPath.Row]);
                return cell;

            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                var btn = new UIButton(UIButtonType.Custom);
                btn.Tag = indexPath.Row;
                _parent.BtnCloseMenuOverlay_TouchUpInside(btn);
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return arrayMenuOptionsTitles.Count;
            }

            public override nint NumberOfSections(UITableView tableView)
            {
                return 1;
            }

            public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
            {
                return 50;
            }
        }
    }
}
