﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;
using System.Linq;
namespace SideMenu.Menu
{
	public class BaseViewController: UIViewController,SlideMenuDelegate
    {
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			addSlideMenuButton();
		}

		protected BaseViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
		}

		public void slideMenuItemSelectedAtIndex(int index)
		{
			UIViewController topViewController = this.NavigationController.TopViewController;

			switch (index)
            {
                case 0:

					this.openViewControllerBasedOnIdentifier("HomeVC");
					break;
        case 1:
					System.Diagnostics.Debug.WriteLine("MyAccountVC\n", "");


					this.openViewControllerBasedOnIdentifier("MyAccountVC");


					break;
        case 2:
					System.Diagnostics.Debug.WriteLine("ChangeCityVC\n",  "");


					this.openViewControllerBasedOnIdentifier("ChangeCityVC");


					break;
        case 3:
					System.Diagnostics.Debug.WriteLine("LatestBLogVC\n",  "");


					this.openViewControllerBasedOnIdentifier("LatestBLogVC");


					break;
        case 4:
					System.Diagnostics.Debug.WriteLine("LiveCHatVC\n",  "");


					this.openViewControllerBasedOnIdentifier("LiveCHatVC");


					break;
        case 5:
					System.Diagnostics.Debug.WriteLine("ContactUsVC\n",  "");


					this.openViewControllerBasedOnIdentifier("ViewController");


					break;
        case 6:
					System.Diagnostics.Debug.WriteLine("ContactUsVC\n", "");


					this.openViewControllerBasedOnIdentifier("ContactUsVC");


					break;
        case 7:
					System.Diagnostics.Debug.WriteLine("PrivacyPolicyVC\n", "");


					this.openViewControllerBasedOnIdentifier("PrivacyPolicyVC");


					break;
        case 8:
					System.Diagnostics.Debug.WriteLine("TermsAndConditoinVC\n", "");


					this.openViewControllerBasedOnIdentifier("TermsAndConditoinVC");


					break;
       
		}


	}

		void openViewControllerBasedOnIdentifier(string _strIdentifier)
        {
			UIViewController destViewController = this.Storyboard.InstantiateViewController(_strIdentifier);


			UIViewController topViewController = this.NavigationController.TopViewController;


			if (topViewController.RestorationIdentifier == destViewController.RestorationIdentifier){
				System.Diagnostics.Debug.WriteLine("Same VC");
                   } 
			else {
				this.NavigationController.PushViewController(destViewController, true);
                  }
        }



		void addSlideMenuButton()
        {
			var btnShowMenu = new UIButton();
			btnShowMenu.SetBackgroundImage(UIImage.FromBundle("menu"), UIControlState.Normal);
			btnShowMenu.Frame = new CGRect(15, 32, 30, 30);
			btnShowMenu.AddTarget(this, new ObjCRuntime.Selector("onSlideMenuButtonPressed:"),UIControlEvent.TouchUpInside);
			this.View.AddSubview(btnShowMenu);
//        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
//        self.navigationItem.leftBarButtonItem = customBarItem;
        }
		UIImage defaultMenuImage()
		{
			var defaultMenuImagee = new UIImage();
			UIGraphics.BeginImageContextWithOptions(new CGSize(30, 22), false, 0.0f);


			UIColor.Black.SetFill();
			UIBezierPath.FromRect(new CGRect(0, 3, 30, 1)).Fill();
			UIBezierPath.FromRect(new CGRect(0, 10, 30, 1)).Fill();
			UIBezierPath.FromRect(new CGRect(0, 17, 30, 1)).Fill();

			UIColor.White.SetFill();
			UIBezierPath.FromRect(new CGRect(0, 4, 30, 1)).Fill();
			UIBezierPath.FromRect(new CGRect( 0,  11,  30, 1)).Fill();
			UIBezierPath.FromRect(new CGRect(0, 18, 30, 1)).Fill();


			defaultMenuImagee = UIGraphics.GetImageFromCurrentImageContext();

			UIGraphics.EndImageContext();
            

        return defaultMenuImagee;
		}
        
		[Export("onSlideMenuButtonPressed:")]
		public void onSlideMenuButtonPressed(UIButton sender)
        {
			if (sender.Tag == 10)
            {
                // To Hide Menu If it already there
				this.slideMenuItemSelectedAtIndex(-1);

				sender.Tag = 0;
                
				UIView viewMenuBack = (from p in View.Subviews select p).Last();

				UIView.Animate(0.3, () =>
				{

					CGRect frameMenu = viewMenuBack.Frame;
					frameMenu.X = -1 * UIScreen.MainScreen.Bounds.Size.Width;
					viewMenuBack.Frame = frameMenu;
					viewMenuBack.LayoutIfNeeded();
					viewMenuBack.BackgroundColor = UIColor.Clear;
				},() => 
				{
					viewMenuBack.RemoveFromSuperview();	
				});

				return;
             }

			sender.Enabled = false;
			sender.Tag = 10;


			MenuViewController menuVC = this.Storyboard.InstantiateViewController("MenuViewController") as MenuViewController;
			menuVC.btnMenu = sender;
			menuVC._delegate = this;
			this.View.AddSubview(menuVC.View);
			this.AddChildViewController(menuVC);
			menuVC.View.LayoutIfNeeded();


            
			menuVC.View.Frame =new CGRect( 0 - UIScreen.MainScreen.Bounds.Size.Width,  0,  UIScreen.MainScreen.Bounds.Size.Width, UIScreen.MainScreen.Bounds.Size.Height);

			UIView.Animate(0.3, () =>
		   {


			   menuVC.View.Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Size.Width, UIScreen.MainScreen.Bounds.Size.Height);
			   sender.Enabled = true;
		   }, null);
        }
}
}